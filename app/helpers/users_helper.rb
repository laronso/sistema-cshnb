module UsersHelper
  def authenticate_user(login_user)
    login_user.email.downcase!
    user = User.where(email: login_user.email, deleted: false, active: true).first
    p user

    unless user.nil?
      if user.password == Digest::MD5.hexdigest(login_user.password)
        session[:user] = user
      else
        return false
      end
    else
      return false
    end
  end

  def user_authenticated?
    session[:user]
  end

  def current_user
    session[:user]
  end

  def end_session_user
    session[:user] = nil
  end

  def change_password(current_password, password, password_confirmation)
    user = User.find(current_user.id)
    if user.password == Digest::MD5.hexdigest(current_password)
      if password == password_confirmation
        user.update_attribute(:password, Digest::MD5.hexdigest(password))
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def usuario_logado
    redirect_to notfound_path if user_authenticated?
  end

  def user_home
    unless current_user.nil?
      unless current_user.user_type_id != 1 || current_user.sector_id != 1 || current_user.durations.last.nil? || current_user.durations.last.function.level != 2
        redirect_to home_patrimonio_path
      end

      unless current_user.user_type_id != 1 || current_user.sector_id != 2 || current_user.durations.last.nil? || current_user.durations.last.function.level != 2
        redirect_to home_transporte_path
      end
    end
  end

  # usuários tipo 1 (funcionários publicos)
  # chefe do setor de patrimonio (espaços) dãããã!
  # def autorizacao_chefe_setor_patrimonio
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 1 || current_user.sector_id != 1 || current_user.durations.last.nil? || current_user.durations.last.function.level != 2
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def chefe_setor_patrimonio?
    #if user_authenticated? && current_user.user_type_id == 1 && current_user.sector_id == 1 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 2
    if user_authenticated? && current_user.sector_id == 1 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 2
      return true
    else   
      return false
    end
  end
  # usuários tipo 1 (funcionários publicos)
  # chefe do setor de transporte! dããã!
  # def autorizacao_chefe_setor_transporte
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 1 || current_user.sector_id != 2 || current_user.durations.last.nil? || current_user.durations.last.function.level != 2
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def chefe_setor_transporte?
    #if user_authenticated? && current_user.user_type_id == 1 && current_user.sector_id == 2 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 2
    if user_authenticated? && current_user.sector_id == 2 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 2
      return true
    else   
      return false
    end
  end
  # usuários tipo 1 (funcionários publicos)
  # professores ou tecnicos administrativos
  # def autorizacao_usuario_nivel_1
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 1 || current_user.durations.last.nil? || current_user.durations.last.function.level != 1
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def usuario_nivel_1?
    if user_authenticated? && current_user.user_type_id == 1 && !current_user.durations.last.nil? && current_user.durations.last.function.level == 1
      return true
    else
      return false
    end
  end
  # usuários tipo 2 (alunos)
  # def autorizacao_alunos
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 2
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def aluno?
    if user_authenticated? && current_user.user_type_id == 2
      return true
    else
      return false
    end
  end

  # usuários tipo 3 (externos a ufpi)
  # def autorizacao_externos
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 3
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def externo?
    if user_authenticated? && current_user.user_type_id == 3
      return true
    else
      return false
    end
  end

  # usuário tipo 1 (funcionário publico)
  # nível 3  (Coordenador Administrativo Financeiro)
  # def autorizacao_caf
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 1 || current_user.durations.last.nil? || current_user.durations.last.function.level != 3
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def caf?
    if user_authenticated? && !current_user.durations.last.nil? && current_user.durations.last.function.level == 3
      return true
    else   
      return false
    end
  end

  # usuário tipo 1 (funcionario publico)
  # nível 4 (diretora do campus)
  # def autorizacao_diretor
  #   if user_authenticated?
  #     redirect_to notfound_path if current_user.user_type_id != 1 || current_user.durations.last.nil? || current_user.durations.last.function.level != 4
  #   else
  #     redirect_to notfound_path
  #   end
  # end

  def diretor?
    if user_authenticated? && !current_user.durations.last.nil? && current_user.durations.last.function.level == 4
      return true
    else   
      return false
    end
  end

  def upper_lvl_3?
    if user_authenticated? && !current_user.durations.last.nil? && current_user.durations.last.function.level >= 3
      return true
    else   
      return false
    end
  end

  def upper_lvl_2?
    if user_authenticated? && !current_user.durations.last.nil? && current_user.durations.last.function.level >= 2
      return true
    else   
      return false
    end
  end

  def code_generate
    a = []
    a += (0..9).to_a
    a += ('a'..'z').to_a
    "#{a[rand(a.size)]}#{a[rand(a.size)]}#{a[rand(a.size)]}"
  end
end