#encoding: utf-8
class ReserveOfPhysicalSpacesController < ApplicationController

  include ReserveOfPhysicalSpacesHelper
  include UsersHelper
  include ApplicationHelper

  # before_filter :autorizacao_chefe_setor_patrimonio, only: [:aprovacoes]

  def index
    @reserve_of_physical_spaces = ReserveOfPhysicalSpace.where(deleted: false, user_id: session[:user].id).order("id DESC")
  end
  
  def espacos
    @spaces = Space.where(deleted: false)
  end
  
  def reservas
    @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id])
    
    @id = params[:id]
    #@reserves = AlocationPermanent.where(deleted: false)
    @array = Array.new(16).map {|e| e = Array.new(31).map {|ee| e = '---' } }

    @reserves.each do |r|
     @array[r.schedule][r.week_day] = 'FIXA'
     @array[r.schedule][r.week_day+7] = 'FIXA'
     @array[r.schedule][r.week_day+14] = 'FIXA'
     @array[r.schedule][r.week_day+21] = 'FIXA'
    end
  end

  def calendario
    mes = params[:mes].to_i
    ano = params[:ano].to_i
    @space = Space.find(params[:id])
    @id = params[:id]
    semester = Semester.where(deleted: false)
    @reserves = []

    semester.each do |s|
      if (s.start_date.year == ano && s.end_date.year == ano) && (mes >= s.start_date.month && mes <= s.end_date.month)
        @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: s.id)        
      elsif (s.start_date.year < ano) && (s.end_date.year == ano) && (mes <= s.end_date.month)
        @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: s.id)
      elsif (s.start_date.year == ano) && (mes >= s.start_date.month)
        @reserves = AlocationPermanent.where(deleted: false, space_id: params[:id], semester_id: s.id)
      end
    end

    @holiday_fixeds = Holiday.where(deleted: false, fixed: true, month: mes)
    @holiday_move = Holiday.where(deleted: false, fixed: false)
    
    @holiday_move = @holiday_move.select { |h| h.date.year == ano }

    @recesso = Recess.where(deleted: false)
    @reserve_of_physical_spaces = ReserveOfPhysicalSpace.where(deleted: false, space_id: params[:id], reservation_status_id: 1)

    @recesso = @recesso.select {|h| h.begin.month == mes || h.end.month == mes }

    @holiday_move = @holiday_move.select {|h| h.date.month == mes} 

    @reserve_of_physical_spaces = @reserve_of_physical_spaces.select {|r| r.date.month == mes && r.date.year == ano}

    x = DateTime.new(ano, mes, 1)
    @qtd_dias_mes = x.end_of_month.day
    @ano = ano
    @mes = mes
    @array = Array.new(16).map {|e| e = Array.new(@qtd_dias_mes).map {|ee| e = '' } }
    
    @reserves.each do |r|
        a = dias_reserva_fixa(mes, ano, r.week_day, @qtd_dias_mes, r.semester.start_date, r.semester.end_date)
        a.each do |w|
          @array[r.schedule][w-1] = r
        end
    end
    
    @recesso.each do |r|
      if r.begin.day >= 1 && r.end.day <= @qtd_dias_mes && (r.begin.year == ano || r.end.year == ano)
        for i in (r.begin.day .. r.end.day)
          for j in (0..15)
            @array[j][i-1] = r
          end
        end
      end

      if (r.begin.year == ano && r.end.year == ano) && (r.begin.month == mes && r.end.month != mes)
         for i in (r.begin.day .. @qtd_dias_mes)
           for j in (0..15)
             @array[j][i-1] = r
           end
         end
      end

      if (r.begin.year == ano && r.end.year == ano) && (r.begin.month != mes && r.end.month == mes)
         for i in (1 .. r.end.day)
           for j in (0..15)
             @array[j][i-1] = r
           end
         end
      end
      

      if (((r.end.day >= 1 && r.begin.month < mes && r.begin.year == ano) || (r.end.day >= 1 && r.begin.month > mes && r.begin.year < ano)) && (r.begin.year == ano - 1))
        for i in (1 .. r.end.day)
          for j in (0..15)
            @array[j][i-1] = r
          end
        end
      end

      if (((r.begin.day >= 1 && r.end.month > mes && r.end.year == ano) || (r.begin.day >= 1 && r.end.month < mes && r.end.year > ano)) && (r.end.year == ano + 1))
        for i in (r.begin.day .. @qtd_dias_mes)
          for j in (0..15)
            @array[j][i-1] = r
          end
        end
      end
    end

    @holiday_fixeds.each do |h|
      for i in (0..15)
        @array[i][h.day - 1] = h
      end
    end


    @holiday_move.each do |h|
      for i in (0..15)
        @array[i][h.date.day - 1] = h
      end
    end
    

    @reserve_of_physical_spaces.each do |r|
      for i in (r.start_time .. r.end_time)
        @array[i][r.date.day - 1] = r
      end
    end
  end

  def show
    @reserve_of_physical_space = ReserveOfPhysicalSpace.find(params[:id])
  end

  def new
    @mes = params[:mes].to_i
    @ano = params[:ano].to_i
    @id = params[:id].to_i
    @reserve_of_physical_space = ReserveOfPhysicalSpace.new(space_id: params[:id])
  end

  def edit
    @reserve_of_physical_space = ReserveOfPhysicalSpace.find(params[:id])
    redirect_to notfound_path if session[:user].id != @reserve_of_physical_space.user_id
    @reserve_of_physical_space.date = format_date_br(@reserve_of_physical_space.date)
  end

  def create
    @mes = params[:mes].to_i
    @ano = params[:ano].to_i
    @id = params[:id].to_i
    
    @reserve_of_physical_space = ReserveOfPhysicalSpace.new(params[:reserve_of_physical_space])
    @reserve_of_physical_space.price = Space.find(@reserve_of_physical_space.space_id).price_local
    @reserve_of_physical_space.deleted = false
    @reserve_of_physical_space.payment_confirmed = false
    @reserve_of_physical_space.user_id = session[:user].id
    @reserve_of_physical_space.reservation_status_id = 2
    
    if @reserve_of_physical_space.save
      redirect_to reserve_of_physical_spaces_path
    else
      render :new
    end
  end

  def aprovacoes
    @reserves = ReserveOfPhysicalSpace.where(deleted: false).order("id DESC")
  end

  def approve
    @reserva = ReserveOfPhysicalSpace.find(params[:id])

    reserves = ReserveOfPhysicalSpace.where(deleted: false, space_id: @reserva.space_id)
    reserves = reserves.select {|reserve| (@reserva.date.day == reserve.date.day && @reserva.date.month == reserve.date.month && @reserva.date.year == reserve.date.year) && ((reserve.start_time >= @reserva.start_time && reserve.start_time <= @reserva.end_time)||(reserve.end_time >= @reserva.start_time && reserve.end_time <= @reserva.end_time)) && reserve.reservation_status_id ==1}

    if reserves.empty? 
      if @reserva.approver != nil
        usuario = User.find(@reserva.approver)
        if session[:user].durations.last.function.level >= usuario.durations.last.function.level
          @reserva.update_attribute(:reservation_status_id, 1)
          @reserva.update_attribute(:approver, session[:user].id)
        else
          flash[:error] = 'Essa reserva foi modificada por um usuário de nivel maior que o seu. Por isso não pode ser modificada'
        end
      else
        @reserva.update_attribute(:reservation_status_id, 1)
        @reserva.update_attribute(:approver, session[:user].id)
      end
    else
      flash[:error] = 'Esse espaço já está reservado'
    end

    redirect_to aprovacoes_path
  end

  def non_approve
    @reserva = ReserveOfPhysicalSpace.find(params[:id])

    if @reserva.approver != nil
      usuario = User.find(@reserva.approver)
      if session[:user].durations.last.function.level >= usuario.durations.last.function.level
        @reserva.update_attribute(:reservation_status_id, 3)
        @reserva.update_attribute(:approver, session[:user].id)
      else
        flash[:error] = 'Essa reserva foi modificada por um usuário de nivel maior que o seu. Por isso não pode ser modificada'
      end
    else
      @reserva.update_attribute(:reservation_status_id, 3)
      @reserva.update_attribute(:approver, session[:user].id)
    end

    redirect_to aprovacoes_path
  end

  def update
    @reserve_of_physical_space = ReserveOfPhysicalSpace.find(params[:id])
    redirect_to notfound_path if session[:user].id != @reserve_of_physical_space.user_id
    if @reserve_of_physical_space.reservation_status_id == 2
      if @reserve_of_physical_space.update_attributes(params[:reserve_of_physical_space])
        redirect_to reserve_of_physical_spaces_path
      else
        render :edit  
      end
    else
      flash[:error] = 'Não é possível modificar uma reserva confirmada ou cancelada'
      render :edit
    end
  end

  def destroy
    @reserve_of_physical_space = ReserveOfPhysicalSpace.find(params[:id])
    @reserve_of_physical_space.update_attribute(:deleted, true)
    redirect_to reserve_of_physical_spaces_path
  end
end
