class SectorsController < ApplicationController

  def index
    @sectors = Sector.all
  end

  def show
    @sector = Sector.find(params[:id])
  end

  def new
    @sector = Sector.new
  end

  def edit
    @sector = Sector.find(params[:id])
  end

  def create
    @sector = Sector.new(params[:sector])

    if @sector.save
      redirect_to sectors_path
    else
      render :new
    end
  end

  def update
    @sector = Sector.find(params[:id])

    if @sector.update_attributes(params[:sector])
      redirect_to sectors_path
    else
      render :edit
    end
  end

  def destroy
    @sector = Sector.find(params[:id])
    @sector.update_attribute(:deleted, true)
    redirect_to sectors_path
  end
end
