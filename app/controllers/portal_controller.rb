class PortalController < ApplicationController
  include UsersHelper
  # before_filter :user_home, only: [:index]

  def index
  end

  def notfound
  end

  def home
  end

  def home_patrimonio
  end

  def home_transporte
  end
end
