class ParticipantsController < ApplicationController
  
  def index
    @participant = User.new
    @participants = Participant.where(deleted: false, request_id: params[:id])
    @id = params[:id]
  end

  def add_participants
    @user = User.where(cpf: params[:user][:cpf]).first

    if @user.nil? == false
      participant = Participant.where(user_id: @user.id, request_id: params[:id]).first
      if participant.nil?
        Participant.create(user_id: @user.id, request_id: params[:id], deleted: false)      
      else
        participant.update_attributes(deleted: false)
      end
    end
    redirect_to add_participants_path(params[:id])
  end

  def show
    @participant = Participant.find(params[:id])
  end

  def new
    @participant = Participant.new
  end

  def edit
    @participant = Participant.find(params[:id])
  end

  def create
    @participant = Participant.new(params[:participant])

    if @participant.save
      redirect_to participants_path
    else
      render :edit
    end
  end

  def update
    @participant = Participant.find(params[:id])

    if @participant.update_attributes(params[:participant])
      redirect_to participants_path
    else
      render :edit
    end
  end

  def destroy
    @participant = Participant.find(params[:id])
    @participant.update_attribute(:deleted, true)
    redirect_to add_participants_path(@participant.request_id)
  end
end
