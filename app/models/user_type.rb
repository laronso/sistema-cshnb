class UserType < ActiveRecord::Base
  has_many :users, :dependent => :destroy
  attr_accessible :type_name, :users_attributes
end
