class Function < ActiveRecord::Base
  has_many :durations, :dependent  => :destroy
  has_many :users, :through => :durations
  has_many :daily_function_locals, dependent: :destroy
  
  attr_accessible :level, :name, :durations_attributes, :users_attributes, :daily_function_locals_attributes

  def name_level
    'Nivel:' + self.level.to_s+' - '+self.name
  end
end
