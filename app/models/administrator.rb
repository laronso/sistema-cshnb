#encoding: utf-8
class Administrator < ActiveRecord::Base
  attr_accessible :email, :name, :password, :password_confirmation
  attr_accessor :password_confirmation

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, presence: true
  validates :email, presence: true,
                    uniqueness: {case_sensitive: false},
                    format: {with: VALID_EMAIL_REGEX}
  validates :password, presence: true
  validates :confirm_password, acceptance: {accept: true, message: 'não coincide com a senha'}

  def confirm_password
    self.password == self.password_confirmation
  end

  before_save do |administrator|
    administrator.email.downcase!
    administrator.password = Digest::MD5.hexdigest(password)
  end
end
