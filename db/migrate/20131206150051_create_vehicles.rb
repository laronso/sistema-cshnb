class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :carrier_plate
      t.string :vehicle_type
      t.integer :number_of_persons
      t.boolean :deleted

      t.timestamps
    end
  end
end
